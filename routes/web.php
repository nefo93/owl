<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'HomeController@index')->name('home');
Route::get('genre/{id}', 'GenreController@show');
Route::get('film/{id}', 'FilmsController@show');
Route::get('new', 'FilmsController@index');
Route::get('top/{top}', 'FilmsController@top');
Route::get('search', 'FilmsController@search');
Route::get('producer/{id}', 'FilmsController@producer');