<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'News';
    public $timestamps = false;
    protected $fillable = ['src', 'img'];
    protected $rules = [
	    	'src' => ['required'],
	    	'img' => ['required'],
	];
}
