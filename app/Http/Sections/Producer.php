<?php

namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
/**
 * Class Producer
 *
 * @property \App\Producer $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Producer extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $model = 'Producer';

    public function initialize()
    {
        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 500, function() {
            return \App\Producer::count();
        });

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //...
        });
    }
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $icon = 'fa fa-male';

    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $table = AdminDisplay::table();
        $table->setHtmlAttribute('class', 'table-primary');
        $table->setColumns(
            AdminColumn::text('id', '#')->setWidth('30px'),
            AdminColumn::link('name', 'Name'),
            AdminColumn::text('data', 'Birthday'),
            AdminColumn::text('description', 'Description'),
            AdminColumn::text('film', 'Film')
        );
        return $table;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::text('name', 'Name')->required(),
            AdminFormElement::text('data', 'Birthday')->required(),
            AdminFormElement::text('description', 'Description')->required(),
            AdminFormElement::text('film', 'Film')->required(),
            AdminFormElement::select('genre_id', 'Genre', \App\Genre::class)->setDisplay(function($option){
                    return $option->name;
            })->required(),
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
