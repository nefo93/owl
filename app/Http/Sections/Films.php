<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumnEditable;
use AdminDisplayFilter;
use App\Admin\Controllers\FilmsController;

/**
 * Class Films
 *
 * @property \App\Films $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Films extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;
    protected $icon = 'fa fa-film';
    protected $controllerClass = FilmsController::class;
    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 500, function() {
            return \App\Films::count();
        });

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {

        });
    }
    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $table = AdminDisplay::table();
        $table->setHtmlAttribute('class', 'table-primary');
        $table->setColumns(
            AdminColumn::text('id', '#'),
            AdminColumn::image('logoImg', 'Image'),
            AdminColumn::link('name', 'Name'),
            AdminColumn::text('Genre.name', 'Genre'),
            AdminColumn::text('year', 'Year'),
            AdminColumn::text('Producer.name', 'Producer'),
            AdminColumn::text('Countrie.name', 'Countrie'),
            AdminColumn::text('Transfer', 'Transfer'),
            AdminColumn::custom('new', function($instance){
                return $instance->new?'<i class="fa fa-check"></i>':'';
            })
        );
        return $table;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::text('name', 'Name')->required(),
            AdminFormElement::select('Genre_id', 'Genre', \App\Genre::class)->setDisplay(function($option){
                    return $option->name;
            }),
            AdminFormElement::file('src', 'Загрузите фильм')->required()->setUploadPath(function(\Illuminate\Http\UploadedFile $file) {
                return 'films';
            }),
            AdminFormElement::text('year', 'Year')->required(),
            AdminFormElement::select('Producer_id', 'Producer', \App\Producer::class)->setDisplay(function($option){
                    return $option->name;
            }),
            AdminFormElement::ckeditor('description', 'Description')->required(),
            AdminFormElement::select('Countries_id', 'Countries', \App\Countries::class)->setDisplay(function($option){
                    return $option->name;
            }),
            AdminFormElement::text('Transfer', 'Transfer')->required(),
            AdminFormElement::checkbox('new','New films')->mutateValue(function($value){
                return $value?'1':'0';
            }),
            AdminFormElement::image('logoImg','Logo Image')->required(),
            AdminFormElement::images('Img', 'Gallery')->setValueSkipped(true),

        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
