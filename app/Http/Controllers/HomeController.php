<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Films;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = Films::simplePaginate(20);
        return view('home', compact('films'));
    }
    public function show($id){
        $films = Films::where('id', $id)->get();
        return view('film.film', compact('films'));
    }
}
