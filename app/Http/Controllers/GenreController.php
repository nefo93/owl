<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Films;

class GenreController extends Controller
{
	public function show($id){
		$films = Films::where('Genre_id', $id)->simplePaginate(20);
		return view('home', compact('films'));
	}
}
