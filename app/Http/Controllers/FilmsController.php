<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Films;
use App\Img;
class FilmsController extends Controller
{
    public function show($id){
        $films = Films::where('id', $id)->get();
        $imgs = Img::where('Films_id', $id)->get();
        return view('film.film', compact('films', 'imgs'));
    }

    public function index(){
    	$films = Films::where('new', '1')->simplePaginate(20);
    	return view('home', compact('films'));
    }
    public function top($top){
    	$films = Films::limit($top)->simplePaginate(20);
    	return view('home', compact('films'));
    }
    public function search(Request $request){
            $referal = trim(strip_tags(stripcslashes(htmlspecialchars($request->search))));
            $films = Films::where('name', 'like', "%{$referal}%")->simplePaginate(20);
            return view('home', compact('films'));    
    }
    public function producer($id){
        $films = Films::where('Producer_id', $id)->simplePaginate(20);
        return view('home', compact('films'));
    }
}
