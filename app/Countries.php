<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'Countries';
    protected $fillable = ['name'];
    protected $rules = [
	    	'name' => ['required'],
	    ];
}
