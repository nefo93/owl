<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producer extends Model
{
    protected $table = 'Producer';
    protected $fillable = [
    	'name',
    	'data',
    	'description',
    	'film',
    ];
    protected $rules = [
	    'name' => ['required'],
	    'data' => ['required'],
    	'description' => ['required'],
    	'film' => ['required'],
	];

}
