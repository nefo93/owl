<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Films extends Model
{
    protected $table = 'Films';
    public $timestamps = false;
    protected $fillable = [
    	'name',
    	'src',
    	'year',
    	'description',
    	'Transfer',
    	'logoImg',
    ];
    protected $rules = [
		'name' => ['required'],
		'src' => ['required'],
    	'year' => ['required'],
    	'description' => ['required'],
    	'Transfer' => ['required'],
    	'logoImg' => ['required'],
	];
    public function Genre(){
        return $this->belongsTo('App\Genre', 'Genre_id');
    }
    public function Producer(){
        return $this->belongsTo('App\Producer', 'Producer_id');
    }
    public function Countrie(){
        return $this->belongsTo('App\Countries', 'Countries_id');
    }
}
