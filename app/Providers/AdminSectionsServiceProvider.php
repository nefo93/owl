<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\User::class => 'App\Http\Sections\Users',
        \App\Genre::class => 'App\Http\Sections\Genres',
        \App\Countries::class => 'App\Http\Sections\Countries',
        \App\Producer::class => 'App\Http\Sections\Producer',
        \App\Films::class => 'App\Http\Sections\Films',
        \App\News::class => 'App\Http\Sections\News',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
