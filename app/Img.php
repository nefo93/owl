<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Img extends Model
{
    protected $table = 'Img';
    public $timestamps = false;
    protected $fillable = ['src', 'Films_id'];
    protected $rules = [
	    	'src' => ['required'],
	    	'Films_id' => ['required'],
	    ];
}
