<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;


class genre extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $Genres = \App\Genre::get();
        return view('widgets.genre', [
            'config' => $this->config,
            'Genres' => $Genres,
        ]);
    }
}
