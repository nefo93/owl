<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Films;
class slider extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $films = Films::where('new' , '1')->get();
        return view('widgets.slider', [
            'config' => $this->config,
            'films' => $films,
        ]);
    }
}
