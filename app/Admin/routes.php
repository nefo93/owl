<?php

Route::get('', ['as' => 'admin.dashboard', function () {
	$title = 'Добро пожаловать в админку!';
	$content = 'Уважаемый админ соблюдайте внимательность и осторожность!';
	return AdminSection::view($content, $title);
}]);

