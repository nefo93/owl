<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Producer', function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('name', 50);
            $table->string('data', 50);
            $table->string('description', 400);
            $table->string('film');
            $table->integer('Genre_id')->unsigned();
            $table->foreign('Genre_id')->references('id')->on('Genre')->odDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Producer');
    }
}
