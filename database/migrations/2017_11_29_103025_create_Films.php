<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Films', function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('name', 50);
            $table->string('src', 100);
            $table->integer('Genre_id')->unsigned();
            $table->foreign('Genre_id')->references('id')->on('Genre')->odDelete('CASCADE')->onUpdate('CASCADE');
            $table->string('year', 50);
            $table->integer('Producer_id')->unsigned();
            $table->foreign('Producer_id')->references('id')->on('Producer')->odDelete('CASCADE')->onUpdate('CASCADE');
            $table->string('description', 5000);
            $table->integer('Countries_id')->unsigned();
            $table->foreign('Countries_id')->references('id')->on('Countries')->odDelete('CASCADE')->onUpdate('CASCADE');
            $table->text('Transfer', 100);
            $table->enum('new', ['0', '1'])->default('0');
            $table->text('logoImg', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Films');
    }
}
