<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Img', function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('src', 200);
            $table->integer('Films_id')->unsigned();
            $table->foreign('Films_id')->references('id')->on('Films')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Img');
    }
}
