@extends('layouts.app')

@section('content')
@foreach($films as $film)

<div class="panel panel-default">
  <div class="panel-heading back"><h1>{{ $film->name }}</h1></div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-5">
  			<img src="{{ asset($film->logoImg) }}" style="width: 100%;" class="hov">
  		</div>
  		<div class="col-md-7 index">
  			<h4>Жанр: <span>{{ $film->Genre->name }}</span></h4>
  			<h4>Год: <span>{{ $film->year }}</span></h4>
  			<h4>Страна: <span>{{ $film->Countrie->name }}</span></h4>
  			<h4>Режиссёр: <a href="{{ url('producer/'.$film->Producer->id.'') }}"><span>{{ $film->Producer->name }}</span></a></h4>
  			<h4>Качество: <span>{{ $film->Transfer }}</span></h4>
  		</div>
  	</div>
  </div>
</div>
<div class="row">
  <div class="col-md-12" style="word-wrap: break-word;">
    {!! $film->description !!}
  </div>
</div>
<hr>

<div class="row marg">
  <div class="col-md-12 ">
    <div id="carousel1" class="panel panel-default"> 
    <ul>
    @foreach($imgs as $img)
      <li><img src="{{ asset($img->src) }}" height="100%"></li>
    @endforeach
    </ul>  
    </div>
  </div>
</div>

<hr>
<div class="panel panel-default">
<div class="row">
  <div class="col-md-12">
    <video id="video1" width="100%" controls="controls" onclick="playPause()">
      <source src="{{ asset($film->src) }}" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' >
      Тег video не поддерживается вашим браузером.
      <a href="{{ asset($film->src) }}">Скачайте видео</a>.
    </video>
  </div>
</div>
</div>
@endforeach
<script language="javascript">
$(document).ready(function(){
  $("#carousel1").tiksluscarousel();  //called with all default values
});

var myVideo = document.getElementById("video1"); 
function playPause() { 
    if (myVideo.paused) 
        myVideo.play(); 
    else 
        myVideo.pause(); 
} 
</script>
@endsection