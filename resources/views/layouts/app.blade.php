<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('name', 'Owl') }}</title>
    <script src="{{ asset('js/app.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/tiksluscarousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css') }}">
    <link href="{{ asset('js/slick/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('js/slick/slick-theme.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/footer-distributed-with-address-and-phones.css') }}">
</head>
<body>
<header>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('images/Owl/owl-1266206_960_720.png') }}">Owl</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li {!! Request::is('/')?'class="active"':'' !!}><a href="{{ url('/') }}">Home</a></li>
        <li {!! Request::is('new')?'class="active"':'' !!}><a href="{{ url('new') }}">New</a></li>
        <li class="dropdown {!! Request::is('top/*')?'active':'' !!}">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Top
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ url('top/100') }}">Топ-100</a></li>
            <li><a href="{{ url('top/50') }}">Топ-50</a></li>
            <li><a href="{{ url('top/10') }}">Топ-10</a></li>
          </ul>
        </li> 
      </ul>
      <form class="navbar-form navbar-left" action="{{ url('/search') }}">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search" name="search">
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <i class="glyphicon glyphicon-search"></i>
            </button>
          </div>
        </div>
      </form>
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::guest())
            <li><a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            <li><a href="{{ route('register') }}"><span class="glyphicon glyphicon-user"></span> Register</a></li>
        @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        <span class="glyphicon glyphicon-log-in"></span>
                            Logout
                        </a>
                        
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    <li><a href="{{ url('admin') }}"><span class="glyphicon glyphicon-user"></span> Admin</a></li>
                </ul>
            </li>
        @endif
      </ul>
    </div>
  </div>
</nav>
</header>
<main class="container">
  <div class="row">
      @section('slider')
        @widget('slider')
        <hr>
      @show
  </div>
  
    <article class="col-md-3 col-sm-12 col-xs-12">
        @section('genre')
            @widget('genre')
        @show
    </article>
    <section class="col-md-7 col-sm-12 col-xs-12">
        @yield('content')
    </section>
    <aside class="col-md-2 col-sm-12 col-xs-12">
        @section('newsbar')
            @widget('news')
        @show
    </aside>
</main>

<footer class="footer-distributed">

      <div class="footer-left">
        
        <h3>O<span>WL</span></h3>
        <p class="footer-links">
          <a href="{{ url('/') }}">Home</a>
          ·
          <a href="{{ url('new') }}">New</a>
          ·
          <a href="{{ url('top/100') }}">Топ-100</a>
          ·
          <a href="{{ url('top/50') }}">Топ-50</a>
          ·
          <a href="{{ url('top/10') }}">Топ-10</a>
        </p>
        
        <p class="footer-company-name">Задание ШАГ &copy; 2015</p>
      </div>

      <div class="footer-center">

        <div>
          <i class="fa fa-map-marker"></i>
          <p><span>Илья</span> Есин</p>
        </div>

        <div>
          <i class="fa fa-phone"></i>
          <p>+38(066)-116-52-24</p>
        </div>

        <div>
          <i class="fa fa-envelope"></i>
          <p><a href="#">nefo933@gmail.com</a></p>
        </div>

      </div>

      <div class="footer-right">

        <p class="footer-company-about">
          <span>About the company</span>
          Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
        </p>

        <div class="footer-icons">

          <a href="#"><i class="fa fa-facebook"></i></a>
          <a href="#"><i class="fa fa-twitter"></i></a>
          <a href="#"><i class="fa fa-linkedin"></i></a>
          <a href="#"><i class="fa fa-github"></i></a>

        </div>

      </div>

    </footer>

    <script src="{{ asset('js/tiksluscarousel.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
</body>
</html>
