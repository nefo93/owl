@extends('layouts.app')

@section('content')
<ul class="film">
<?php $i = 0; ?> 
@foreach($films as $film)
<?php $i++ ?>

<li class="li"><a href="{{ url('film/'.$film->id) }}">
	<div class="img" style="background-image: url({{ asset($film->logoImg) }});">	
	</div>
	<div class="text">
		{{ $film->name}}
	</div></a>
</li>
@if($i%4 == 0)
	<hr style="clear: left;">
@endif
@endforeach
</ul>
<div class="row lef">
	{{$films->links()}}
</div>

@endsection