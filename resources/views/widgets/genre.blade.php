<div class="list-group">
@foreach($Genres as $g)
	<a href="{{url('genre/'.$g->id.'')}}" class="list-group-item {!! Request::is('genre/'.$g->id)?'active':'' !!} animated flip hover">{{ $g->name }} <span class="badge">{{ \App\Films::where('Genre_id', $g->id)->count()}}</span></a>

	
@endforeach 
</div>