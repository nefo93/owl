<div data-slick='{"slidesToShow": 4, "slidesToScroll": 4}' class="slider">	
  @foreach($films as $film)
  <a href="{{url('film/'.$film->id.'')}}" class="csf">
  <div style="margin: 10px; padding: 5px;" class="di">
      <img src="{{ asset($film->logoImg) }}" width="100%" height="330px" class="img">
      <h4 style="text-align: center;">{{$film->name}}</h4>
  </div>
  </a>
  @endforeach
</div>





<script src="{{ asset('js/slick/slick.min.js') }}"></script>
<script type="text/javascript">
        $(".slider").slick({

  // normal options...
  infinite: false,
        slidesToShow: 1,
        autoplay: true,
        slidesToScroll: 1,
        infinite: true,
  // the magic
  responsive: [{

      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        autoplay: true,
        slidesToScroll: 1,
        infinite: true
      }

    }, {

      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        autoplay: true,
        slidesToScroll: 1,
        dots: true
      }

    }, {

      breakpoint: 300,
      settings: "unslick" // destroys slick

    }]
});

</script>